from django.contrib import admin
from .models import Canal, Cursos, Videos

# Register your models here.
admin.site.register(Canal)
admin.site.register(Cursos)
admin.site.register(Videos)
